# Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!

Workers of the world, unite!

Пролетарии всех стран, соединяйтесь!

https://tehnokom.su


# vue-block-editor
```
Block editor with collumns and JSON output for Vue.js and Nuxt.js
```

## Install

### Vue.js
```
npm i vue-block-editor

In main.js:

import editor from "vue-block-editor";

Vue.component("app-editor", editor);
```

### Nuxt.js
```
npm i vue-block-editor

In folder 'plugins' create file editor.js
In editor.js:

import Vue from "vue";

import editor from "vue-block-editor";

Vue.component("app-editor", editor);

In nuxt.config.js:
  plugins: [{ src: "@/plugins/editor", ssr: false }],
```

### Usage
```
<template>
  <div style="width: 800px;">
    <h1>VUE-BLOCK-EDITOR</h1>
    <app-editor :language="ru_RU" ref="editor" @save="on_save_method" @onChange="on_change_method" />
  </div>
</template>

<script>
export default {
  methods: {
    on_save_method(value) {
      console.log(value);
    },
    on_change_method(value) {
      console.log(value);
    }
  },
  mounted() {
    this.$refs.editor.save();
  }
};


```

### Customize configuration
```
In prop "language" you can choose one of two languages: English(en_GB), Russian(ru_RU)
```